package passengertransport.routingtickets.model

import java.math.BigDecimal
import java.util.*

/**
 * Class representing bus ticket
 */
class Ticket {
    var route: String = ""
        private set

    var fromStop: String = ""
        private set

    var toStop: String = ""
        private set

    var tariff: String = ""
        private set

    var price: BigDecimal = 0.toBigDecimal()
        private set

    var date: Calendar = Calendar.getInstance()
        private set

    /**
     * Constructor
     *
     * Generates date by creation date
     *
     * @param stage Stage ticket is generated for
     */
    constructor(stage: Stage) {
        route = stage.route.number
        fromStop = stage.fromStop.name
        toStop = stage.toStop.name
        tariff = stage.tariff.name

        price = stage.price
    }

    /**
     * Constructor for state loading
     *
     * @param route_ Route number
     * @param from First stop
     * @param to Last stop
     * @param tariff_ Tariff name
     * @param price_ Ticket price
     * @param date_ Sell date
     */
    internal constructor(
        route_: String,
        from: String,
        to: String,
        tariff_: String,
        price_: BigDecimal,
        date_: Calendar
    ) {
        route = route_
        fromStop = from
        toStop = to
        tariff = tariff_
        price = price_
        date = date_
    }
}
