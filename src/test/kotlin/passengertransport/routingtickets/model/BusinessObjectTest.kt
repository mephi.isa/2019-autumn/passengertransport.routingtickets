package passengertransport.routingtickets.model

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.security.InvalidParameterException
import java.util.*


class TariffTest {

    @Test
    fun tariffValidConstructorTest() {
        val t = Tariff("Tariff35", 3, 5)
        assertEquals(t.name, "Tariff35")
        assertEquals(t.fromZone, 3)
        assertEquals(t.toZone, 5)
        assertEquals(t, Tariff("Tariff35", 3, 5))
    }

    @Test(expected = IllegalArgumentException::class)
    fun tariffInvalidConstructorTest() {
        Tariff("Tariff35", 5, 3)
    }

    @Test(expected = IllegalArgumentException::class)
    fun tariffInvalidEqConstructorTest() {
        Tariff("Tariff35", 3, 3)
    }

    @Test
    fun tariffCompareTest() {
        val t = Tariff("t1", 1, 2)
        val t1 = Tariff("t1", 1, 2)
        val t2 = Tariff("t1", 0, 2)
        val t3 = Tariff("t1", 1, 3)
        val t4 = Tariff("t2", 1, 2)
        val t5: Any? = "t"
        val t6: Any? = null
        assertTrue(t == t1)
        assertTrue(t != t2)
        assertTrue(t != t3)
        assertTrue(t != t4)
        assertTrue(t.hashCode() == t1.hashCode())
        assertTrue(t.hashCode() != t2.hashCode())
        assertTrue(t != t5)
        assertTrue(t != t6)
    }

    @Test
    fun tariffCheckZoneTest() {
        val t = Tariff("t1", -5, 17)

        assertTrue(t.checkZone(-5))
        assertTrue(t.checkZone(-3))
        assertTrue(t.checkZone(10))
        assertTrue(t.checkZone(16))

        assertTrue(!t.checkZone(17))
        assertTrue(!t.checkZone(-6))
    }
}

class StopTest {

    private val z: Int = 5

    @Test
    fun stopValidConstructorTest() {
        val s = Stop("Abc", 1.2f, 3.4f, z)
        assertEquals(s.name, "Abc")
        assertTrue(s.latitude - 1.2 < 0.0001)
        assertTrue(s.longitude - 3.4 < 0.0001)
        assertEquals(s.zone, z)
    }

    @Test
    fun stopValidConstructorNegTest() {
        val s = Stop("Abc", -89.2f, -179.4f, z)
        assertEquals(s.name, "Abc")
        assertTrue(s.latitude - -89.2 < 0.0001)
        assertTrue(s.longitude - -179.4 < 0.0001)
    }

    @Test(expected = InvalidParameterException::class)
    fun stopInvalidLatTest() {
        Stop("Abc", 95.2f, 0.0f, z)
    }


    @Test(expected = InvalidParameterException::class)
    fun stopInvalidNegLatTest() {
        Stop("Abc", -95.2f, 0.0f, z)
    }

    @Test(expected = InvalidParameterException::class)
    fun stopInvalidLonTest() {
        Stop("Abc", 0.0f, 195.2f, z)
    }

    @Test(expected = InvalidParameterException::class)
    fun stopInvalidNegLonTest() {
        Stop("Abc", 0.0f, -195.2f, z)
    }

    @Test
    fun stopCompareTrivialTest() {
        val s = Stop("Abc", 0.0f, -95.2f, z)
        assertTrue(s == s)
        assertTrue(s.hashCode() == s.hashCode())
    }

    @Test
    fun stopCompareValidTest() {
        val s1 = Stop("Abc", 0.0f, -95.2f, z)
        val s2 = Stop("Abc", 0.0f, -95.2f, z)
        assertTrue(s1 == s2)
        assertTrue(s1.hashCode() == s2.hashCode())
    }

    @Test
    fun stopCompareInvalidTest() {

        val s1 = Stop("Abc", 0.0f, -95.2f, z)
        val s2 = Stop("Abc_", 0.0f, -95.2f, z)
        val s3 = Stop("Abc", 1.0f, -95.2f, z)
        val s4 = Stop("Abc", 0.0f, 95.2f, z)
        assertTrue(s1 != s2)
        assertTrue(s1 != s3)
        assertTrue(s1 != s4)
        assertTrue(s1.hashCode() != s2.hashCode())
        assertTrue(s1.hashCode() != s3.hashCode())
        assertTrue(s1.hashCode() != s4.hashCode())
    }

    @Test
    fun stopCompareDifferentClassTest() {

        val s1 = Stop("Abc", 0.0f, -95.2f, z)
        val s2: Any? = "Stop(\"Abc\", 0.0f, -95.2f)"
        assertTrue(s1 != s2)
        assertTrue(s1.hashCode() != s2.hashCode())
    }

    @Test
    fun stopCompareNullTest() {

        val s1 = Stop("Abc", 0.0f, -95.2f, z)
        val s2 = null
        assertTrue(s1 != s2)
        assertTrue(s1.hashCode() != s2.hashCode())
    }
}

class StageTest {
    private val route = Route(
        "209m", "Tram 209m",
        mutableListOf(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), Stop("S2", 2.0f, 2.0f, 3))
    )
    private val tariff = Tariff("Basic", 0, 10)

    @Test
    fun stageValidConstructorTest() {
        val s = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), tariff)
        assertTrue(s.fromStop == route.getStops()[0])
        assertTrue(s.toStop == route.getStops()[1])
        assertTrue(s.route == route)
        assertTrue(s.price == 3.14.toBigDecimal())
        assertTrue(s.tariff == Tariff("Basic", 0, 10))
        assertTrue(s.isActual())
    }

    @Test
    fun stageValidArchiveConstructorTest() {
        val s = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), tariff, false)
        assertTrue(s.fromStop == route.getStops()[0])
        assertTrue(s.toStop == route.getStops()[1])
        assertTrue(s.route == route)
        assertTrue(s.price == 3.14.toBigDecimal())
        assertTrue(s.tariff == Tariff("Basic", 0, 10))
        assertTrue(!s.isActual())
    }

    @Test
    fun stageArchiveTest() {
        val s1 = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), tariff)
        val s2 = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), tariff, false)
        assertTrue(s1.isActual())
        assertTrue(!s2.isActual())
        s1.markArchive()
        s2.markArchive()
        assertTrue(!s1.isActual())
        assertTrue(!s2.isActual())
    }

    @Test(expected = InvalidParameterException::class)
    fun stageInvalidRouteConstructorTest() {
        Stage(Stop("S5", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), tariff)
    }

    @Test(expected = InvalidParameterException::class)
    fun stageInvalidTariff1ConstructorTest() {
        Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), Tariff("Basic", 9, 10))
    }

    @Test(expected = InvalidParameterException::class)
    fun stageInvalidTariff2ConstructorTest() {
        Stage(
            Stop("S0", 0.0f, 0.0f, 15),
            Stop("S1", 1.0f, 1.0f, 3),
            route,
            3.14f.toBigDecimal(),
            Tariff("Basic", 9, 10)
        )
    }

    @Test(expected = InvalidParameterException::class)
    fun stageInvalidTariff3ConstructorTest() {
        Stage(Stop("S0", 0.0f, 0.0f, 9), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), Tariff("Basic", 9, 10))
    }

    @Test(expected = InvalidParameterException::class)
    fun stageInvalidTariff4ConstructorTest() {
        Stage(
            Stop("S0", 0.0f, 0.0f, 9),
            Stop("S1", 1.0f, 1.0f, 15),
            route,
            3.14f.toBigDecimal(),
            Tariff("Basic", 9, 10)
        )
    }

    @Test(expected = InvalidParameterException::class)
    fun stageInvalidPriceConstructorTest() {
        Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, (-3.14f).toBigDecimal(), tariff)
    }

    @Test
    fun stageCompareTest() {
        val r2 = Route("209k", "Tram 209k", mutableListOf(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3)))
        val t2 = Tariff("Advanced", 3, 4)

        val s = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), tariff)
        val s0 = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), tariff)
        val s1 = Stage(Stop("S2", 2.0f, 2.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), tariff)
        val s2 = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S2", 2.0f, 2.0f, 3), route, 3.14f.toBigDecimal(), tariff)
        val s3 = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), r2, 3.14f.toBigDecimal(), tariff)
        val s4 = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.15f.toBigDecimal(), tariff)
        val s5 = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), t2)
        val s6: Any? = null
        val s7: Any? = "Stage"

        assertTrue(s == s0)
        assertTrue(s != s1)
        assertTrue(s != s2)
        assertTrue(s != s3)
        assertTrue(s == s4)
        assertTrue(s != s5)
        assertTrue(s != s6)
        assertTrue(s != s7)

        assertTrue(s.hashCode() == s0.hashCode())
        assertTrue(s.hashCode() != s1.hashCode())
    }
}


class RouteTest {
    @Test
    fun plainConstructorTest() {
        val r = Route("209m", "Tram 209m")
        assertTrue(r.number == "209m")
        assertTrue(r.name == "Tram 209m")
        assertTrue(r.getStops().isEmpty())
        assertTrue(r.getStages().isEmpty())
    }

    @Test
    fun fullConstructorTest() {
        val r = Route(
            "209m", "Tram 209m",
            mutableListOf(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), Stop("S2", 2.0f, 2.0f, 3))
        )
        val tariff = Tariff("Advanced", 3, 4)

        r.loadStage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), 3.14f.toBigDecimal(), tariff)
        r.loadStage(Stop("S1", 1.0f, 1.0f, 3), Stop("S0", 0.0f, 0.0f, 3), 3.14f.toBigDecimal(), tariff, false)

        assertTrue(r.number == "209m")
        assertTrue(r.name == "Tram 209m")
        assertTrue(
            r.getStops() == listOf(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), Stop("S2", 2.0f, 2.0f, 3))
        )
        assertTrue(
            r.getStages() == setOf(
                Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), r, 3.14f.toBigDecimal(), tariff),
                Stage(Stop("S1", 1.0f, 1.0f, 3), Stop("S0", 0.0f, 0.0f, 3), r, 3.14f.toBigDecimal(), tariff)
            )
        )
    }


    @Test
    fun compareTest() {
        val r = Route(
            "209m", "Tram 209m",
            mutableListOf(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), Stop("S2", 2.0f, 2.0f, 3))
        )
        val tariff = Tariff("Advanced", 3, 4)

        r.loadStage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), 3.14f.toBigDecimal(), tariff)
        r.loadStage(Stop("S1", 1.0f, 1.0f, 3), Stop("S0", 0.0f, 0.0f, 3), 3.14f.toBigDecimal(), tariff, false)

        assertTrue(r.number == "209m")
        assertTrue(r.name == "Tram 209m")
        assertTrue(
            r.getStops() == listOf(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), Stop("S2", 2.0f, 2.0f, 3))
        )
        assertTrue(
            r.getStages() == setOf(
                Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), r, 3.14f.toBigDecimal(), tariff),
                Stage(Stop("S1", 1.0f, 1.0f, 3), Stop("S0", 0.0f, 0.0f, 3), r, 3.14f.toBigDecimal(), tariff)
            )
        )

        val r2 = Route(
            "209m", "Tram 209m",
            mutableListOf(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), Stop("S2", 2.0f, 2.0f, 3))
        )

        val r3 = Route("209m", "Tram 209m")
        val r4 = Route("209m", "Tram 209")
        val r5 = Route("209", "Tram 209m")
        val r6: Any? = "Route"
        val r7: Any? = null

        assertTrue(r == r2)
        assertTrue(r == r3)
        assertTrue(r != r4)
        assertTrue(r != r5)
        assertTrue(r != r6)
        assertTrue(r != r7)
        assertTrue(r.hashCode() == r2.hashCode())
        assertTrue(r.hashCode() != r4.hashCode())
    }

    @Test
    fun addStopTest() {
        val dfl = Tariff("Default", 0, 10)
        val high = Tariff("High", 6, 10)
        val mid = Tariff("Mid", 3, 8)
        val low = Tariff("Low", 0, 4)
        Route.tariffSet.add(high)
        Route.tariffSet.add(mid)
        Route.tariffSet.add(low)
        Route.tariffSet.add(dfl)
        val route = Route("209m", "Tram 209m")
        val stop1 = Stop("A", 0.0f, 0.0f, 5)
        val stop2 = Stop("B", 0.0f, 0.0f, 5)
        val stop3 = Stop("C", 0.0f, 0.0f, 5)


        assertTrue(!route.addStop(5, stop3))


        assertTrue(route.getStages().isEmpty())
        assertTrue(route.addStop(0, stop1))
        assertTrue(route.getStages().isEmpty())
        assertTrue(route.addStop(1, stop2))


        assertTrue(route.getStages().contains(Stage(stop1, stop2, route, 0.toBigDecimal(), dfl)))
        assertTrue(route.getStages().contains(Stage(stop2, stop1, route, 0.toBigDecimal(), dfl)))
        assertTrue(route.getStages().contains(Stage(stop1, stop2, route, 0.toBigDecimal(), mid)))
        assertTrue(route.getStages().contains(Stage(stop2, stop1, route, 0.toBigDecimal(), mid)))

        assertTrue(route.getStages().size == 4)

        route.addStop(1, stop3)

        assertEquals(route.getStops(), listOf(stop1, stop3, stop2))
    }

    @Test
    fun removeStopTest() {
        val dfl = Tariff("Default", 0, 10)
        Route.tariffSet.add(dfl)
        val route = Route("209m", "Tram 209m")
        val stop1 = Stop("A", 0.0f, 0.0f, 5)
        val stop2 = Stop("B", 0.0f, 0.0f, 5)
        val stop3 = Stop("C", 0.0f, 0.0f, 5)

        route.addStop(0, stop1)
        route.addStop(1, stop2)
        route.addStop(2, stop3)
        assertEquals(route.getStops(), listOf(stop1, stop2, stop3))

        assertTrue(route.getStages().contains(Stage(stop1, stop2, route, 0.toBigDecimal(), dfl)))
        assertTrue(route.getStages().contains(Stage(stop2, stop1, route, 0.toBigDecimal(), dfl)))
        assertTrue(route.getStages().contains(Stage(stop1, stop3, route, 0.toBigDecimal(), dfl)))
        assertTrue(route.getStages().contains(Stage(stop3, stop1, route, 0.toBigDecimal(), dfl)))
        assertTrue(route.getStages().contains(Stage(stop3, stop2, route, 0.toBigDecimal(), dfl)))
        assertTrue(route.getStages().contains(Stage(stop2, stop3, route, 0.toBigDecimal(), dfl)))


        route.removeStop(1)
        assertEquals(route.getStops(), listOf(stop1, stop3))

        assertTrue(route.getStages().contains(Stage(stop1, stop3, route, 0.toBigDecimal(), dfl)))
        assertTrue(route.getStages().contains(Stage(stop3, stop1, route, 0.toBigDecimal(), dfl)))

        assertTrue(route.getStages().size == 2)

        route.removeStop(0)
        assertEquals(route.getStops(), listOf(stop3))
        assertTrue(route.getStages().isEmpty())
    }

    @Test
    fun removeStopFailTest() {
        val dfl = Tariff("Default", 0, 10)
        Route.tariffSet.add(dfl)
        val route = Route("209m", "Tram 209m")
        val stop1 = Stop("A", 0.0f, 0.0f, 5)
        val stop2 = Stop("B", 0.0f, 0.0f, 5)
        val stop3 = Stop("C", 0.0f, 0.0f, 5)

        route.addStop(0, stop1)
        route.addStop(1, stop2)
        route.addStop(2, stop3)
        assertEquals(route.getStops(), listOf(stop1, stop2, stop3))

        assertTrue(!route.removeStop(3))
        assertTrue(!route.removeStop(-10))
        assertTrue(!route.removeStop(-1))

        assertTrue(route.removeStop(0))
        assertEquals(route.getStops(), listOf(stop2, stop3))
    }

    @Test
    fun updatePriceTest() {
        val dfl = Tariff("Default", 0, 10)
        val dfl2 = Tariff("Default2", 0, 10)
        Route.tariffSet.add(dfl)
        val route = Route("209m", "Tram 209m")
        val stop1 = Stop("A", 0.0f, 0.0f, 5)
        val stop2 = Stop("B", 0.0f, 0.0f, 5)

        route.addStop(0, stop1)
        route.addStop(1, stop2)
        assertEquals(route.getStops(), listOf(stop1, stop2))

        assertTrue(route.getStages().contains(Stage(stop1, stop2, route, 0.toBigDecimal(), dfl)))
        assertTrue(route.getStages().contains(Stage(stop2, stop1, route, 0.toBigDecimal(), dfl)))

        assertTrue(route.updatePrice(stop1, stop2, dfl, 5.toBigDecimal()))
        for (stage in route.getStages()) {
            if (stage.fromStop == stop1 && stage.toStop == stop2) {
                assertTrue(stage.price == 5.toBigDecimal())
            } else {
                assertTrue(stage.price == 0.toBigDecimal())
            }
        }

        assertTrue(!route.updatePrice(stop1, stop2, dfl2, 5.toBigDecimal()))
    }
}

class TicketTest {

    private val route = Route(
        "209m", "Tram 209m",
        mutableListOf(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), Stop("S2", 2.0f, 2.0f, 3))
    )
    private val tariff = Tariff("Basic", 0, 10)
    private val stage = Stage(Stop("S0", 0.0f, 0.0f, 3), Stop("S1", 1.0f, 1.0f, 3), route, 3.14f.toBigDecimal(), tariff)

    @Test
    fun normalConstructorTest() {
        val ticket = Ticket(stage)
        assertTrue(ticket.route == route.number)
        assertTrue(ticket.fromStop == stage.fromStop.name)
        assertTrue(ticket.toStop == stage.toStop.name)
        assertTrue(ticket.tariff == tariff.name)
        assertTrue(ticket.price == stage.price)
    }

    @Test
    fun dbConstructorTest() {
        val date = Calendar.getInstance()
        val ticket = Ticket("14", "London", "Paris", "cheap", 1.3.toBigDecimal(), date)
        assertTrue(ticket.route == "14")
        assertTrue(ticket.fromStop == "London")
        assertTrue(ticket.toStop == "Paris")
        assertTrue(ticket.tariff == "cheap")
        assertTrue(ticket.price == 1.3.toBigDecimal())
        assertTrue(ticket.date == date)
    }
}