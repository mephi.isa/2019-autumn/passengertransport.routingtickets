package passengertransport.routingtickets.model

import java.math.BigDecimal
import java.security.InvalidParameterException


/**
 * Stage class
 *
 * Represents value of travelling between two stops
 *
 * @param fromStop First stop
 * @param toStop Last stop
 * @param route Related bus route
 * @param price Ride price
 * @param tariff Related tariff
 * @param actual Actual/archive flag
 *
 */
class Stage internal constructor(
    val fromStop: Stop,
    val toStop: Stop,
    val route: Route,
    val price: BigDecimal,
    val tariff: Tariff,
    private var actual: Boolean = true
) {


    init {
        if (!tariff.checkZone(fromStop.zone)
            || !tariff.checkZone(toStop.zone)
        ) {
            throw InvalidParameterException("Incorrect tariff used")
        }

        if (!route.getStops().containsAll(listOf(fromStop, toStop))) {
            throw InvalidParameterException("Route does not contain given stops")
        }

        if (price < 0.toBigDecimal()) {
            throw InvalidParameterException("Price should be positive")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other != null && other is Stage) {
            val otherS: Stage = other
            return otherS.fromStop == fromStop && otherS.toStop == toStop && otherS.route == route && otherS.tariff == tariff
        }
        return false
    }

    override fun hashCode(): Int {
        return fromStop.hashCode() + toStop.hashCode() + tariff.hashCode() + route.hashCode()
    }

    /**
     * Actuality check method
     */
    fun isActual(): Boolean {
        return actual
    }

    /**
     * Mark the stage archive method
     */
    fun markArchive() {
        actual = false
    }
}