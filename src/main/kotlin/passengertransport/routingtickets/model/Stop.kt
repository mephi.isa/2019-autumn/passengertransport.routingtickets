package passengertransport.routingtickets.model

import java.security.InvalidParameterException


/**
 * Class representing buss stop
 *
 * @param name Stop name
 * @param latitude Location latitude
 * @param longitude Location longitude
 * @param zone Tariff zone of the stop
 */
class Stop(
    val name: String,
    val latitude: Float,
    val longitude: Float,
    val zone: Int
) {
    init {
        if (latitude < -90.0 || latitude > 90.0) {
            throw InvalidParameterException("Latitude is out of range")
        }

        if (longitude < -180.0 || longitude > 180.0) {
            throw InvalidParameterException("Longitude is out of range")
        }
    }

    @Override
    override fun equals(other: Any?): Boolean {
        if (other != null && other is Stop) {
            val otherS: Stop = other
            return otherS.name == name && otherS.latitude == latitude && otherS.longitude == longitude && otherS.zone == zone
        }
        return false
    }

    @Override
    override fun hashCode(): Int {
        return name.hashCode() + latitude.hashCode() + longitude.hashCode() + zone.hashCode()
    }
}
