package passengertransport.routingtickets.model


/**
 * Class representing tariff
 *
 * @param name Tariff name
 * @param fromZone Least included zone
 * @param toZone Least not included zone
 */
data class Tariff(
    val name: String,
    val fromZone: Int,
    val toZone: Int
) {
    init {
        require(fromZone < toZone) { "fromZone should be less than toZone" }
    }

    /**
     * Zone check method
     *
     * Checks whether tariff applies to the zone
     * @param zone Zone number
     */
    fun checkZone(zone: Int): Boolean {
        return zone in fromZone until toZone
    }
}