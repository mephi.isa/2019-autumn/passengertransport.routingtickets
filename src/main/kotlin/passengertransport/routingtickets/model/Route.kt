package passengertransport.routingtickets.model

import java.math.BigDecimal
import java.util.stream.Collectors

/**
 * This class represents a bus route
 *
 */
class Route {

    /**
     * Public constructor
     *
     * @param number_ Bus route number
     * @param name_ Bus route name, may be different from number
     */
    constructor(number_: String, name_: String) {
        number = number_
        name = name_
    }

    /**
     * Constructor for state loading
     *
     * @param number_ Bus route number
     * @param name_ Bus route name, may be different from number
     * @param stops_ List of stops, stages won't be generated
     */
    internal constructor(
        number_: String,
        name_: String,
        stops_: MutableList<Stop>
    ) {
        number = number_
        name = name_
        stops = stops_
    }

    var number: String = ""
        private set

    var name: String = ""
        private set

    private var stops: MutableList<Stop> = mutableListOf()

    private var stages: MutableSet<Stage> = mutableSetOf()

    /**
     * Immutable getter for stops list
     *
     * @return list of stops of the route
     */
    fun getStops(): List<Stop> {
        return stops
    }


    /**
     * Stage addition method for state loading
     *
     * @param from First stop of the stage
     * @param to Last stop of the stage
     * @param price price of the ticket for the stage
     * @param tariff used tariff
     * @param actual whether stage could be used for routing or not
     */
    internal fun loadStage(from: Stop, to: Stop, price: BigDecimal, tariff: Tariff, actual: Boolean = true) {
        stages.add(Stage(from, to, this, price, tariff, actual))
    }

    /**
     * New stop addition method
     *
     * Adds stops and all the possible stages
     *
     * @param pos Position to add the stop
     * @param newStop Stop to add
     *
     * @return true if added successfully
     */
    fun addStop(pos: Int, newStop: Stop): Boolean {
        if (pos > stops.size)
            return false

        stops.add(pos, newStop)
        for (stop in stops) {
            if (stop != newStop) {
                for (tariff in tariffSet) {
                    if (stop.zone >= tariff.fromZone
                        && stop.zone < tariff.toZone
                        && newStop.zone >= tariff.fromZone
                        && newStop.zone < tariff.toZone
                    ) {
                        stages.add(Stage(stop, newStop, this, 0.toBigDecimal(), tariff))
                        stages.add(Stage(newStop, stop, this, 0.toBigDecimal(), tariff))
                    }
                }
            }
        }

        return true
    }

    /**
     * Stop removal method
     *
     * Removes stop and all the associated stages
     *
     * @param pos Position to add the stop
     *
     * @return true if removed successfully
     */
    fun removeStop(pos: Int): Boolean {
        if (pos >= 0 && stops.size > pos) {
            val stop = stops[pos]
            stages = stages.stream()
                .filter { stage -> stage.fromStop != stop && stage.toStop != stop }
                .collect(Collectors.toSet())
            stops.removeAt(pos)
            return true
        }
        return false
    }

    /**
     * Immutable getter for stages set
     *
     * @return set of route stages
     */
    fun getStages(): Set<Stage> {
        return stages
    }

    /**
     * Price update method
     *
     * Updates price of the given stage
     *
     * @param fromStop First stop of the stage
     * @param toStop Last stop of the stage
     * @param tariff Tariff of the stage
     * @param price New price
     *
     * @return true if stage was found
     */
    fun updatePrice(fromStop: Stop, toStop: Stop, tariff: Tariff, price: BigDecimal): Boolean {
        val stage = Stage(fromStop, toStop, this, price, tariff)

        if (stages.remove(stage)) {
            return stages.add(stage)
        }
        return false
    }

    /**
     * Equals
     */
    override fun equals(other: Any?): Boolean {
        if (other != null && other is Route) {
            val otherR: Route = other
            return otherR.name == name && otherR.number == number
        }
        return false
    }

    /**
     * Hash code
     */
    override fun hashCode(): Int {
        return name.hashCode() + number.hashCode()
    }

    companion object {

        /**
         * Static set of available tariffs
         */
        var tariffSet: MutableSet<Tariff> = mutableSetOf()
    }
}